--- sample & hold
-- in1: sampling clock
-- out1: random sample

function init()
	input[1].mode('change',1.0,0.1,'rising')
	output[2].volts = 3
	output[1].slew = 0.2
	output[2].slew = 0.5
end


input[1].change = function(state)
    rand = math.random() * 5 - 2
    output[1].volts = rand
    
    rand = math.random() * 3 + 1
    output[2].volts = rand
    
end

